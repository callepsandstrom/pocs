import Vue from 'vue';

import Uttagsplan from './apps/uttagsplan/main';
import Katt from './apps/katt/main';
import Hej from './apps/hej/main';
import Fyran from './apps/fyran/main';

const apps = [
    Uttagsplan,
    Katt,
    Hej,
    Fyran
];

apps.forEach(app => {
    if (document.querySelector(app.el)) {
        new Vue(app);
    }
});