import Vue from 'vue';
import Header from '../../shared/header.vue';

export default {
    el: '#hej',
    data() {
        return {
            title: ''
        }
    },
    mounted() {
        (this as any).title = 'Tja (direkt från instansen)';
    }
}