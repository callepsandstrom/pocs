import Vue from 'vue';

import Fyran from './fyran';
import Header from '../../shared/header.vue';

export default {
    el: '#fyran',
    components: {
        'app-fyran': Fyran
    }
}