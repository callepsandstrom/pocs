import Uttagsplan from './uttagsplan.vue';
import store from './store';

export default {
    el: '#uttagsplan',
    components: {
        'app-uttagsplan': Uttagsplan
    },
    store
}