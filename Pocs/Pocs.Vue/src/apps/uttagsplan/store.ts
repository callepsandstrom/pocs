import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const initialState: UttagsplanState = {
    count: 0
}

export default new Vuex.Store({
    state: initialState,
    mutations: {
        increment(state: any) {
            state.count++
        }
    }
});

interface UttagsplanState {
    count: number;
}