import Vue from 'vue';

import Katt from './katt';
import Header from '../../shared/header.vue';

export default {
    el: '#katt',
    components: {
        'app-katt': Katt,
        'app-header': Header
    }
}