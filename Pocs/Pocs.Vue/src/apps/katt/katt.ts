import { Vue, Component } from "vue-property-decorator";
import { Djur } from "../../models/djur";

@Component
export default class Katt extends Vue {
  djur: Djur = { typ: '' };

  mounted() {
    this.djur = { typ: "Katt" };
  }
}