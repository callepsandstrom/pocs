﻿using Microsoft.AspNetCore.Mvc;

namespace Pocs.Vue.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Uttagsplan()
        {
            return View();
        }
    }
}
