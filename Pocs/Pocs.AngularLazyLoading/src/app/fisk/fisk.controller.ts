import { IController } from "angular";
import { FiskService } from "../shared/services/fisk.service";

export default class FiskController implements IController {
    
    static $inject = ["$scope", "fiskService"];

    message: string;

    constructor(private readonly $scope: any, private readonly fiskService: FiskService) { }
    
    $onInit() {
        this.$scope.message = this.fiskService.getFisk();
    }
}
