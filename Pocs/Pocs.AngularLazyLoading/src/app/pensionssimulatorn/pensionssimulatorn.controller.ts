import { IController } from "angular";
import { FiskService } from "../shared/services/fisk.service";

export default class PensionssimulatornController implements IController {
    
    static $inject = ["$scope"];

    title: string;

    constructor(private readonly $scope: any) { }
    
    $onInit() {
        this.$scope.title = 'Pensionssimulatorn'
    }
}
