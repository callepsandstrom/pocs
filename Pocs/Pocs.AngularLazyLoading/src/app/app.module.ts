import * as angular from 'angular';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef, SystemJsNgModuleLoader } from '@angular/core';
import { UpgradeModule, downgradeInjectable } from '@angular/upgrade/static';
import { RouterModule } from '@angular/router';

import PensionssimulatornController from './pensionssimulatorn/pensionssimulatorn.controller';
import FiskController from './fisk/fisk.controller';

import { UttagsplanModule } from './uttagsplan/uttagsplan.module';
import { NavbarModule } from './navbar/navbar.module';

import { FiskService } from './shared/services/fisk.service';
import { FactoryService } from '../factory.service';
import { AppStore } from './app.store';

import { routes } from './routes';

@NgModule({
  imports: [
    BrowserModule,
    UpgradeModule,
    RouterModule.forChild(routes),
    UttagsplanModule,
    NavbarModule
  ],
  providers: [FiskService, FactoryService, SystemJsNgModuleLoader, AppStore]
})
export class AppModule {
  constructor(private upgrade: UpgradeModule, private factoryService: FactoryService) { }

  ngDoBootstrap(applicationRef: ApplicationRef) {

    angular.module('PensionssimulatornModule', [])
      .controller('PensionssimulatornController', PensionssimulatornController);

    angular.module('FiskModule', [])
      .factory('fiskService', downgradeInjectable(FiskService))
      .controller('FiskController', FiskController);

    this.factoryService.getFactories().then(factories => {
      factories.forEach(factory => {
        if (factory.js) {
          this.upgrade.bootstrap(factory.element, [factory.moduleName], { strictDi: true });
        } else {
          applicationRef.bootstrap(factory);
        }
      });
    });
  }
}
