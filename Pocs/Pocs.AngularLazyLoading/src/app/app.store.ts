import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AppStore {

    private countSubject = new BehaviorSubject<number>(0);

    count$ = this.countSubject.asObservable();

    constructor() { }

    increment() {
        this.countSubject.next(this.countSubject.value + 1);
    }
}