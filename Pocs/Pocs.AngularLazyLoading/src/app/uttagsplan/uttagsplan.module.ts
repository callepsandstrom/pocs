import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UttagsplanComponent } from './uttagsplan.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UttagsplanComponent],
  entryComponents: [UttagsplanComponent]
})
export class UttagsplanModule { }
