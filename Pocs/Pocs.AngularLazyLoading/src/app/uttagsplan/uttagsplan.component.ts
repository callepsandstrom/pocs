import { Component, OnInit } from '@angular/core';
import { FiskService } from '../shared/services/fisk.service';
import { AppStore } from '../app.store';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-uttagsplan',
  templateUrl: './uttagsplan.component.html'
})
export class UttagsplanComponent implements OnInit {

  count$: Observable<number>;
  title: string = "Uttagsplan";

  constructor(private fiskService: FiskService, private appStore: AppStore) { }

  ngOnInit() {
    this.count$ = this.appStore.count$;
    console.log(this.fiskService.getFisk());
  }

  increment() {
    this.appStore.increment();
  }
}
