import { Routes } from "@angular/router";
import { UttagsplanComponent } from "./uttagsplan/uttagsplan.component";
import { NavbarComponent } from "./navbar/navbar.component";

export const routes: Routes = [
    { loadChildren: './uttagsplan/uttagsplan.module#UttagsplanModule', component: UttagsplanComponent },
    { loadChildren: './navbar/navbar.module#NavbarModule', component: NavbarComponent }
];
