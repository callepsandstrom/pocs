import { Component, OnInit } from '@angular/core';
import { FiskService } from '../shared/services/fisk.service';
import { Observable } from 'rxjs/Observable';
import { AppStore } from '../app.store';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

    count: number;
  
  constructor(private appStore: AppStore) { }

  ngOnInit() {
    this.appStore.count$.subscribe(count => this.count = count);
  }
}
