import { Injectable, Injector, SystemJsNgModuleLoader, NgModuleFactory, ComponentFactory } from '@angular/core';
import { Route } from '@angular/router';
import { routes } from './app/routes';

@Injectable()
export class FactoryService {

    constructor(private injector: Injector, private loader: SystemJsNgModuleLoader) { }

    getFactories(): Promise<any[]> {
        return Promise.all(this.appElements.map(appElement => this.mapToFactoryPromise(appElement)));
    }

    private get appElements() {
        return Array.from(document.querySelectorAll('*'))
            .filter(element => element.hasAttribute('mip-module'))
            .map(element => {
                return {
                    element,
                    moduleName: element.getAttribute('mip-module'),
                    js: element.hasAttribute('ng-controller')
                };
            });
    }

    private mapToFactoryPromise(appElement: any): Promise<any> {
        return appElement.js ? appElement : this.getComponentFactory(appElement.moduleName);
    }

    private getComponentFactory(moduleName: string): Promise<ComponentFactory<any>> {
        const route = this.getRoute(moduleName);

        return this.loader.load(route.loadChildren.toString()).then((moduleFactory: NgModuleFactory<any>) => {
            const moduleRef = moduleFactory.create(this.injector);
            return moduleRef.componentFactoryResolver.resolveComponentFactory(route.component);
        });
    }

    private getRoute(name: string): Route {
        return routes.find(r => r.loadChildren.toString().includes(name));
    }
}
