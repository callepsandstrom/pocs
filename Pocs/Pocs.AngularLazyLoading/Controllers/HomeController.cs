using Microsoft.AspNetCore.Mvc;

namespace Pocs.AngularLazyLoading.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Pensionssimulatorn()
    {
      return View();
    }

    public IActionResult Uttagsplan()
    {
      return View();
    }
  }
}
