using Microsoft.AspNetCore.Mvc;

namespace Pocs.AngularMPA.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Pensionssimulatorn()
    {
      return View();
    }

    public IActionResult Uttagsplan()
    {
      return View();
    }
  }
}
