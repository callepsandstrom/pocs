import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/delay'
import 'rxjs/add/operator/map'

import { PrognosChartData, PrognosPeriod2, Prognosperiod } from '../models/prognos';
import { PensionssimulatornChartData, PensionssimulatornPeriod } from '../models/pensionssimulatorn';

@Injectable()
export class ChartDataService {

    constructor(private http: HttpClient) { }

    getChartData(number: number): Observable<PensionssimulatornChartData> {
        return this.http.get<PrognosChartData>(`../assets/chart-data-${number}.json`)
            .map(chartData => this.getPensionssimulatornChartData(chartData))
            .delay(1000);
    }

    private getPensionssimulatornChartData(chartData: PrognosChartData): PensionssimulatornChartData {
        return {
            expectedLifeExpectancy: chartData.forvantadLivslangd,
            periods: this.getPeriods(chartData)
        }
    }

    private getPeriods(chartData: PrognosChartData): PensionssimulatornPeriod[] {
        return [
            this.getFirstPeriod(chartData.prognos.currentAlderAr, chartData.prognos.prognosperiodList[0].fromAlderYear, chartData.prognos.currentManadslon),
            ...this.getMiddlePeriods(chartData.prognos.prognosperiodList),
            this.getLastPeriod(chartData.toYear, chartData.prognos.prognosperiodList[chartData.prognos.prognosperiodList.length - 1])
        ]
    }

    private getFirstPeriod(from: number, to: number, salary: number): PensionssimulatornPeriod {
        return {
            from,
            to,
            length: to - from > 7 ? 7 : to - from,
            incomes: this.getIncomes(salary, null)
        }
    }

    private getMiddlePeriods(prognosPeriods: Prognosperiod[]): PensionssimulatornPeriod[] {
        return prognosPeriods.filter((prognos, index) => index !== prognosPeriods.length - 1).map((period, index) => {
            return {
                from: period.fromAlderYear,
                to: prognosPeriods[index + 1] ? prognosPeriods[index + 1].fromAlderYear : prognosPeriods[prognosPeriods.length - 1].fromAlderYear,
                length: prognosPeriods[index + 1] ? (prognosPeriods[index + 1].fromAlderYear - period.fromAlderYear) : prognosPeriods[prognosPeriods.length - 1].fromAlderYear - period.fromAlderYear,
                incomes: this.getIncomes(0, period)
            };
        });
    }

    private getLastPeriod(to: number, period: Prognosperiod): PensionssimulatornPeriod {
        return {
            from: period.fromAlderYear,
            to,
            length: to - period.fromAlderYear === 0 ? 1 : (to - period.fromAlderYear),
            incomes: this.getIncomes(0, period)
        }
    }

    private getIncomes(salary: number, period: Prognosperiod) {
        return {
            salary,
            generalPension: period ? period.pensionPerManad.ap.totalt : 0,
            privatePension: period ? period.pensionPerManad.pps.totalt + period.pensionPerManad.pps.totaltEgeninmatning : 0,
            servicePension: period ? period.pensionPerManad.tjp.totalt + period.pensionPerManad.tjp.totaltEgeninmatning : 0,
        }
    }
}

