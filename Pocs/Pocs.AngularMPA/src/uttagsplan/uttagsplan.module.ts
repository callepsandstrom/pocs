import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { UttagsplanComponent } from './uttagsplan.component';
import { Steg1Component } from './steg-1/steg-1.component';
import { Steg2Component } from './steg-2/steg-2.component';
import { PensionssimulatornChartComponent } from '../pensionssimulatorn/chart/pensionssimulatorn-chart.component';
import { ChartDataService } from '../services/chart-data.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    UttagsplanComponent,
    Steg1Component,
    Steg2Component,
    PensionssimulatornChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ChartDataService],
  bootstrap: [UttagsplanComponent]
})
export class UttagsplanModule { }
