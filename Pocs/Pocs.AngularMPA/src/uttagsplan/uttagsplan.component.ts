import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uttagsplan',
  templateUrl: './uttagsplan.component.html',
  styleUrls: ['./uttagsplan.component.scss']
})
export class UttagsplanComponent implements OnInit {
  title = 'Uttagsplan';
  currentStep: number = 1;

  constructor() { }

  ngOnInit() {
  }

  changeStep(step: number) {
    this.currentStep = step;
  }
}
