import { Component, OnInit } from '@angular/core';

import { ChartDataService } from '../../services/chart-data.service';
import { PrognosChartData } from '../../models/prognos';
import { PensionssimulatornChartData } from '../../models/pensionssimulatorn';

@Component({
  selector: 'app-steg-1',
  templateUrl: './steg-1.component.html',
  styleUrls: ['./steg-1.component.scss'],
})
export class Steg1Component implements OnInit {
  modalInfo: any;
  currentPrognos: number = 3;
  loading: boolean;
  chartData: PensionssimulatornChartData;
  detailedView: boolean;

  constructor(private chartDataService: ChartDataService) { }

  ngOnInit(): void {
    this.loadChartData(this.currentPrognos);
  }

  loadChartData(number: number): void {
    this.currentPrognos = number;
    this.loading = true;
    this.chartDataService.getChartData(number).subscribe(data => {
      this.chartData = data;
      this.loading = false;
    });
  }

  handleOnClick(point: any): void {
    this.modalInfo = point.index === 0 ? this.getSalaryModalInfo(point) : this.getPensionModalInfo(point);
  }

  private getSalaryModalInfo(point: any) {
    return {
      header: `Din lön mellan ${point.category}: ${this.chartData.periods[0].incomes.salary} kr/mån`,
      footer: 'Detta är den månadslön vi använt för att beräkna din pension, baserat på den lön vi har registrerad.'
    }
  }

  private getPensionModalInfo(point: any) {
    const category = point.index === this.chartData.periods.length - 1 ? point.category.toLowerCase() : `mellan ${point.category}`;
    const incomes = this.chartData.periods[point.index].incomes;

    return {
      header: `Din pensionsutbetalning ${category}: ${point.stackTotal} kr/mån`,
      pensions: [
        { key: 'Allmän pension', value: incomes.generalPension },
        { key: 'Tjänstepension', value: incomes.servicePension },
        { key: 'Privat pension', value: incomes.privatePension }
      ],
      footer: 'Beloppen är i svenska kronor före skatt och motsvarar dagens penningvärde.'
    }
  }
}
