export interface PensionssimulatornChartData {
    expectedLifeExpectancy: number;
    periods: PensionssimulatornPeriod[]
}

export interface PensionssimulatornPeriod {
    from: number;
    to: number;
    length: number;
    incomes: PensionssimulatornIncomes
}

export interface PensionssimulatornIncomes {
    salary: number;
    generalPension: number;
    privatePension: number;
    servicePension: number;
}