import { Löneantagandetyp } from "../enums/enums";

export interface Prognos {
    currentAlderAr: number;
    currentManadslon: number;
    finalManadslon: number;
    forstaPensionsutbetalningen: number;
    forvantadLivslangd: number;
    prognosperiodList: Prognosperiod[];
    loneantagandetyp: Löneantagandetyp;
    inrapporteradManadslonInstitutNamn: string;
    livsvarig: boolean;
}

export interface Prognosperiod {
    fromAlderYear: number;
    fromAlderMonth: number;
    pensionPerManad: PensionPerManad;
    isLivsvarig: boolean;
}

export interface Pensionsprognos {
    pensionsalderSimulering: Pensionsalder;
    defaultPensionsalder: number;
    defaultLon: number;
    pensionsalderHasChanged: boolean;
    prognosHasSimulering: boolean;
    lonOrJobbHasChanged: boolean;
    sistaLon: number;
    loneantagandetyp: Löneantagandetyp;
    inrapporteradManadslonInstitutNamn: string;
    forstaPensionsutbetalningen: number;
    diffSek: number;
    diffPercent: number;
    alder: number;
    chart: PrognosChartData;
}

export interface PrognosChartData {
    prognos: Prognos;
    fromYear: number;
    toYear: number;
    forvantadLivslangd: number;
    showDifference: boolean;
}

export interface PensionPerManadTjpPps {
    totalt: number;
    totaltEgeninmatning: number;
    prognostypBelopp: PensionstypBelopp[];
}

export interface PensionPerManadAp {
    totalt: number;
    prognostypBelopp: PensionstypBelopp[];
}

export interface PensionPerManad {
    ap: PensionPerManadAp;
    tjp: PensionPerManadTjpPps;
    pps: PensionPerManadTjpPps;
}

export interface PensionstypBelopp {
    namn: string;
    belopp: number;
}

export interface PrognosPeriod2 {
    from: number;
    to: number;
    length: number;
}

export class Pensionsalder {
    availableYears: number[];

    constructor(public floor: number,
        public ceil: number,
        public value: number) {
        this.fillAvailableYears();
    }

    setÅldersspann(floor: number, ceil: number) {
        this.floor = floor;
        this.ceil = ceil;

        this.fillAvailableYears();
    }

    private fillAvailableYears(): void {
        this.availableYears = new Array<number>();

        for (let i = this.floor; i <= this.ceil; i++) {
            this.availableYears.push(i);
        }
    }
}