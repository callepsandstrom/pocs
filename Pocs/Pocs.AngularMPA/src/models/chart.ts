export interface VariwideOptions extends Highcharts.Options {
    chart: VariwideChartOptions;
    plotOptions?: VariwidePlotOptions;
}

export interface VariwideChartOptions extends Highcharts.ChartOptions {
    events: VariwideChartEvents;
}

export interface VariwideChartEvents extends Highcharts.ChartEvents {
    render: any;
}

export interface VariwidePlotOptions extends Highcharts.PlotOptions {
    variwide: any;
}