import { IController } from "angular";
import { ChartDataService } from "../services/chart-data.service";

export default class PensionssimulatornController implements IController {

    static $inject = ['$scope', 'chartService'];

    constructor(private readonly $scope: any, private readonly chartDataService: ChartDataService) { }

    $onInit() {
        this.$scope.title = 'Pensionssimulatorn'
        this.$scope.loading = true;
        this.chartDataService.getChartData(1).subscribe(chartData => {
            this.$scope.data = chartData;
            this.$scope.loading = false;
        });
    }
}
