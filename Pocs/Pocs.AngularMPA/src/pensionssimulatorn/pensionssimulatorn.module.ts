import * as angular from 'angular';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UpgradeModule, downgradeInjectable, downgradeComponent } from '@angular/upgrade/static';
import { HttpClientModule } from '@angular/common/http';

import PensionssimulatornController from './pensionssimulatorn.controller';
import { PensionssimulatornChartComponent } from './chart/pensionssimulatorn-chart.component';
import { ChartDataService } from '../services/chart-data.service';

@NgModule({
  imports: [
    BrowserModule,
    UpgradeModule,
    HttpClientModule
  ],
  declarations: [PensionssimulatornChartComponent],
  providers: [ChartDataService],
  entryComponents: [PensionssimulatornChartComponent]
})
export class PensionssimulatornModule {

  constructor(private upgrade: UpgradeModule) { }

  ngDoBootstrap() {
    angular.module('pensionssimulatorn-module', [])
      .factory('chartService', downgradeInjectable(ChartDataService))
      .directive('pensionssimulatornChart', downgradeComponent({
        component: PensionssimulatornChartComponent
      }) as angular.IDirectiveFactory)
      .controller('pensionssimulatorn-controller', PensionssimulatornController);

    this.upgrade.bootstrap(document.querySelector('#pensionssimulatorn'), ['pensionssimulatorn-module'], { strictDi: true });
  }
}
