import { Component, Input, ElementRef, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';

import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/modules/variwide')(Highcharts);

import { PrognosChartData, PrognosPeriod2 } from '../../models/prognos';
import { VariwideOptions } from '../../models/chart';
import { PensionssimulatornChartData } from '../../models/pensionssimulatorn';

@Component({
    selector: 'pensionssimulatorn-chart',
    templateUrl: './pensionssimulatorn-chart.component.html',
    styleUrls: ['./pensionssimulatorn-chart.component.scss']
})
export class PensionssimulatornChartComponent {
    @Input() chartData: PensionssimulatornChartData;
    @Input() detailedView: boolean;

    @Output() onClick = new EventEmitter();

    constructor(private elementRef: ElementRef) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.chartData && changes.chartData.currentValue) {
            this.createChart();
        }

        if (changes.detailedView && this.chartData) {
            this.createChart();
        }
    }

    private get series(): any[] {
        return this.detailedView ? this.detailedSeries : this.normalSeries
    }

    private get normalSeries(): any[] {
        return [this.lönSerie, this.pensionSerie];
    }

    private get detailedSeries(): any[] {
        return [this.lönSerie, this.privataPensionerSerie, this.tjänstepensionSerie, this.allmänaPensionerSerie];
    }

    private get lönSerie() {
        return {
            name: 'Lön',
            color: 'rgba(168,99,186, .96)',
            data: this.chartData.periods.map(p => [p.incomes.salary, p.length])
        };
    }

    private get pensionSerie() {
        return {
            name: 'Pension',
            color: 'rgba(14,210,201, .95)',
            data: this.chartData.periods.map(p => [p.incomes.generalPension + p.incomes.privatePension + p.incomes.servicePension, p.length])
        };
    }

    private get privataPensionerSerie() {
        return {
            name: 'Privat pension',
            color: 'rgba(56,198,113, .95)',
            data: this.chartData.periods.map(p => [p.incomes.privatePension, p.length])
        };
    }

    private get tjänstepensionSerie() {
        return {
            name: 'Tjänstepension',
            color: 'rgba(33,134,249, .95)',
            data: this.chartData.periods.map(p => [p.incomes.servicePension, p.length])
        };
    }

    private get allmänaPensionerSerie() {
        return {
            name: 'Allmän pension',
            color: 'rgba(255,59,83, .95)',
            data: this.chartData.periods.map(p => [p.incomes.generalPension, p.length])
        };
    }

    private get categories(): string[] {
        return this.chartData.periods.map((period, index) => {
            if (index === this.chartData.periods.length - 1) {
                return `Från ${period.from} år`
            }
            return `${period.from}-${period.to} år`
        });
    }

    private get options(): VariwideOptions {
        return {
            chart: {
                type: 'variwide',
                backgroundColor: 'transparent',
                marginRight: 45,
                events: {
                    redraw: e => this.removeCustomElements(),
                    render: e => this.renderCustomElements(e.target)
                }
            },
            title: {
                text: null
            },
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    style: {
                        color: 'white'
                    },
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            xAxis: {
                categories: this.categories,
                tickLength: 0,
                labels: {
                    rotation: -45,
                    style: {
                        color: 'white'
                    }
                }
            },
            plotOptions: {
                variwide: {
                    stacking: 'normal',
                    borderColor: 'transparent',
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: e => this.onClick.emit(e.point)
                        }
                    }
                },
                line: {
                    pointPlacement: null,
                    color: 'white',
                    visible: false,
                    dashStyle: 'Dash',
                    enableMouseTracking: false,
                    lineWidth: 1
                }
            },
            tooltip: {
                shared: true,
                style: {
                    display: 'none'
                }
            },
            legend: {
                itemStyle: {
                    color: 'white'
                }
            },
            credits: {
                enabled: false
            },
            series: this.series
        }
    }

    private createChart(): void {
        new Highcharts.Chart(this.elementRef.nativeElement, this.options);
    }

    private renderCustomElements(chart: any) {
        this.renderBrokenXAxis(chart);
        this.renderExpectedLifeExpectancyMarker(chart);
        this.renderLifetimeTriangles(chart);
        // this.renderComparingPath(chart);
    }

    private removeCustomElements() {
        Array.from(document.getElementsByClassName('custom-element')).forEach(element => {
            if (element) {
                element.remove();
            }
        });
    }

    private renderBrokenXAxis(chart: any) {
        const x = chart.plotLeft + chart.series[0].points[0].shapeArgs.width / 2;
        const y = chart.plotTop + chart.plotHeight;

        chart.renderer.rect(x - 10, y - 10, 20, 20, 25).attr({
            'stroke-width': 2,
            stroke: 'black',
            fill: 'white',
            zIndex: 3,
            class: 'custom-element'
        }).add();
    }

    private renderExpectedLifeExpectancyMarker(chart: any) {
        const percentage =
            (this.chartData.expectedLifeExpectancy - this.chartData.periods[0].from) /
            (this.chartData.periods[this.chartData.periods.length - 1].to - this.chartData.periods[0].from);

        const x = chart.plotLeft + chart.plotWidth * percentage;
        const y = chart.plotTop + chart.plotHeight;

        chart.renderer.rect(x - 10, y - 10, 20, 20, 25).attr({
            'stroke-width': 2,
            stroke: 'black',
            fill: 'yellow',
            zIndex: 3,
            class: 'custom-element'
        }).add();
    }

    private renderLifetimeTriangles(chart: any) {
        const points = chart.series
            .map((serie, index) => {
                return {
                    color: serie.color,
                    shapeArgs: serie.points[serie.points.length - 1].shapeArgs,
                    [`x${index}`]: 0,
                    [`x${index}`]: .33
                }
            })
            .filter(point => point.shapeArgs.height);

        const x = chart.plotLeft + points[0].shapeArgs.x + points[0].shapeArgs.width;
        const y = chart.plotTop + points[0].shapeArgs.y;
        const height = chart.plotTop + chart.plotHeight - y;
        const middle = height / 2 + y;
        const width = 25;

        let linearGradient = {};
        let stops = [];

        points.forEach((point, index) => {
            const previousY = index === 0 ? 0 : stops[index][0];
            const y = point.shapeArgs.height / height + previousY;
            linearGradient[`x${index + 1}`] = 0;
            linearGradient[`y${index + 1}`] = index;
            stops.push([previousY, point.color], [y, point.color]);
        });

        const coordinates = [x, y, x, y + height, x + width, middle];

        chart.renderer.path(['M', ...coordinates, 'Z']).attr({
            fill: { linearGradient, stops },
            zIndex: 3,
            class: 'custom-element'
        }).add();
    }

    private renderComparingPath(chart: any) {

        const periods = [
            [35000, 7],
            [10000, 10],
            [40000, 5],
            [33000, 5],
            [25000, 1],
        ];

        const yAxis = chart.yAxis[0];
        const totalZ = this.chartData.periods.reduce((a, b) => a + b.length, 0);

        let points = [];
        let startPoints = [];

        periods.forEach((period, index) => {
            const previousX = index === 0 ? chart.plotLeft : points[index - 1][0];
            const x = previousX + chart.plotWidth * period[1] / totalZ;
            const y = yAxis.toPixels(period[0]);

            startPoints.push([previousX, y]);
            points.push([x, y]);
        });

        const coordinates = points.map((point, index) => [startPoints[index], point]);

        chart.renderer.path(['M', startPoints[0][0], startPoints[0][1], 'L', ...coordinates]).attr({
            'stroke-width': 2,
            'stroke-dasharray': '5',
            stroke: 'white',
            zIndex: 3,
            id: 'comparingPath',
            class: 'custom-element'
        }).add();
    }
}